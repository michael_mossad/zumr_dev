System.register(['@angular/core', "./login/login.component", "./header/header.component", "./sideMenu/sideMenu.component", "./footer/footer.component", './globals'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, login_component_1, header_component_1, sideMenu_component_1, footer_component_1, myGlobals;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (header_component_1_1) {
                header_component_1 = header_component_1_1;
            },
            function (sideMenu_component_1_1) {
                sideMenu_component_1 = sideMenu_component_1_1;
            },
            function (footer_component_1_1) {
                footer_component_1 = footer_component_1_1;
            },
            function (myGlobals_1) {
                myGlobals = myGlobals_1;
            }],
        execute: function() {
            //import {testRoutComponent} from "./testRout/testRout.component";
            //import {RouteConfig, ROUTER_PROVIDERS, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
            //import {ProfileComponent} from "./profile/profile.component";
            AppComponent = (function () {
                function AppComponent() {
                }
                //    this0templateVisibility = true;
                AppComponent.prototype.ngOnInit = function () {
                    console.log("-----------------------");
                    console.log("myGlobals");
                    console.log(myGlobals.templateVisibility);
                    this.templateVisibility = myGlobals.templateVisibility;
                    myGlobals.templateVisibility;
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n    <login></login>\n        \n       \n    ",
                        directives: [login_component_1.LoginComponent, header_component_1.HeaderComponent, sideMenu_component_1.SideMenuComponent, footer_component_1.FooterComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFRQSxrRUFBa0U7WUFDbEUsOEZBQThGO1lBQzlGLCtEQUErRDtZQWdDL0Q7Z0JBQUE7Z0JBY0EsQ0FBQztnQkFiRCxxQ0FBcUM7Z0JBQ2pDLCtCQUFRLEdBQVI7b0JBRUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFBO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUN0QyxJQUFJLENBQUMsa0JBQWtCLEdBQUUsU0FBUyxDQUFDLGtCQUFrQixDQUFDO29CQUMxRCxTQUFTLENBQUMsa0JBQWtCLENBQUM7Z0JBQzdCLENBQUM7Z0JBdkNMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFFBQVEsRUFBRSxnREFJVDt3QkFDRCxVQUFVLEVBQUUsQ0FBQyxnQ0FBYyxFQUFDLGtDQUFlLEVBQUMsc0NBQWlCLEVBQUMsa0NBQWUsQ0FBQztxQkFHakYsQ0FBQzs7Z0NBQUE7Z0JBa0NGLG1CQUFDO1lBQUQsQ0FkQSxBQWNDLElBQUE7WUFkRCx1Q0FjQyxDQUFBIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLy88cmVmZXJlbmNlIHBhdGg9XCIuLi9ub2RlX21vZHVsZXMvQGFuZ3VsYXIvcm91dGVyLWRlcHJlY2F0ZWQvcm91dGVyLmQudHNcIi8+XHJcbmltcG9ydCB7Q29tcG9uZW50LE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7TG9naW5Db21wb25lbnR9IGZyb20gXCIuL2xvZ2luL2xvZ2luLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge0hlYWRlckNvbXBvbmVudH0gZnJvbSBcIi4vaGVhZGVyL2hlYWRlci5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtTaWRlTWVudUNvbXBvbmVudH0gZnJvbSBcIi4vc2lkZU1lbnUvc2lkZU1lbnUuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7Rm9vdGVyQ29tcG9uZW50fSBmcm9tIFwiLi9mb290ZXIvZm9vdGVyLmNvbXBvbmVudFwiO1xyXG5cclxuaW1wb3J0IG15R2xvYmFscyA9IHJlcXVpcmUoJy4vZ2xvYmFscycpO1xyXG4vL2ltcG9ydCB7dGVzdFJvdXRDb21wb25lbnR9IGZyb20gXCIuL3Rlc3RSb3V0L3Rlc3RSb3V0LmNvbXBvbmVudFwiO1xyXG4vL2ltcG9ydCB7Um91dGVDb25maWcsIFJPVVRFUl9QUk9WSURFUlMsIFJPVVRFUl9ESVJFQ1RJVkVTfSBmcm9tICdAYW5ndWxhci9yb3V0ZXItZGVwcmVjYXRlZCc7XHJcbi8vaW1wb3J0IHtQcm9maWxlQ29tcG9uZW50fSBmcm9tIFwiLi9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50XCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXktYXBwJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICA8bG9naW4+PC9sb2dpbj5cclxuICAgICAgICBcclxuICAgICAgIFxyXG4gICAgYCxcclxuICAgIGRpcmVjdGl2ZXM6IFtMb2dpbkNvbXBvbmVudCxIZWFkZXJDb21wb25lbnQsU2lkZU1lbnVDb21wb25lbnQsRm9vdGVyQ29tcG9uZW50XVxyXG4vLyAgICBwcm92aWRlcnM6IFtST1VURVJfUFJPVklERVJTXVxyXG5cclxufSlcclxuXHJcbi8vZXhwb3J0IGNvbnN0IHRlbXBsYXRlVmlzaWJpbGl0eSBcclxuXHJcblxyXG4vL0BSb3V0ZUNvbmZpZyhbXHJcbi8vICB7XHJcbi8vICAgIHBhdGg6ICcvbG9naW4nLFxyXG4vLyAgICBuYW1lOiAnTG9naW4nLFxyXG4vLyAgICBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50LFxyXG4vLyAgICB1c2VBc0RlZmF1bHQ6IHRydWVcclxuLy8gIH0sXHJcbi8vICB7XHJcbi8vICAgIHBhdGg6ICcvdGVzdFJvdXQnLFxyXG4vLyAgICBuYW1lOiAnVGVzdFJvdXQnLFxyXG4vLyAgICBjb21wb25lbnQ6IHRlc3RSb3V0Q29tcG9uZW50XHJcbi8vICB9XHJcbi8vXSlcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgQXBwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0e1xyXG4vLyAgICB0aGlzMHRlbXBsYXRlVmlzaWJpbGl0eSA9IHRydWU7XHJcbiAgICBuZ09uSW5pdCgpXHJcbiAgICB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cIikgIFxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIm15R2xvYmFsc1wiKVxyXG4gICAgY29uc29sZS5sb2cobXlHbG9iYWxzLnRlbXBsYXRlVmlzaWJpbGl0eSk7XHJcbiAgICAgICAgdGhpcy50ZW1wbGF0ZVZpc2liaWxpdHk9IG15R2xvYmFscy50ZW1wbGF0ZVZpc2liaWxpdHk7XHJcbiAgICBteUdsb2JhbHMudGVtcGxhdGVWaXNpYmlsaXR5O1xyXG4gICAgfVxyXG4vLyAgICBjb25zb2xlLmxvZyhcIi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXCIpXHJcbi8vICAgIGNvbnNvbGUubG9nKFwibXlHbG9iYWxzXCIpXHJcbi8vICAgIGNvbnNvbGUubG9nKG15R2xvYmFscyk7XHJcbi8vICAgIHRoaXMubXlHbG9iYWxzLnRlbXBsYXRlVmlzaWJpbGl0eTtcclxufSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
