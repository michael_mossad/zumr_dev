System.register(["@angular/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SideMenuComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            //import { Router } from '@angular/router-deprecated';
            SideMenuComponent = (function () {
                function SideMenuComponent() {
                }
                SideMenuComponent = __decorate([
                    core_1.Component({
                        selector: "side-menu",
                        templateUrl: "dev/sideMenu/sideMenu.tpl.html"
                    }), 
                    __metadata('design:paramtypes', [])
                ], SideMenuComponent);
                return SideMenuComponent;
            }());
            exports_1("SideMenuComponent", SideMenuComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNpZGVNZW51L3NpZGVNZW51LmNvbXBsb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFFQSxzREFBc0Q7WUFRdEQ7Z0JBQUE7Z0JBRUEsQ0FBQztnQkFSRDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBQyxXQUFXO3dCQUNwQixXQUFXLEVBQUMsZ0NBQWdDO3FCQUUvQyxDQUFDOztxQ0FBQTtnQkFJRix3QkFBQztZQUFELENBRkEsQUFFQyxJQUFBO1lBRkQsaURBRUMsQ0FBQSIsImZpbGUiOiJzaWRlTWVudS9zaWRlTWVudS5jb21wbG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG4vL2ltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlci1kZXByZWNhdGVkJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6XCJzaWRlLW1lbnVcIixcclxuICAgIHRlbXBsYXRlVXJsOlwiZGV2L3NpZGVNZW51L3NpZGVNZW51LnRwbC5odG1sXCJcclxuLy8gICAgc3R5bGVVcmxzOltcImFzc2V0cy9wYWdlcy9jc3MvbG9naW4tNS5jc3NcIl1cclxufSlcclxuXHJcbmV4cG9ydCAgY2xhc3MgU2lkZU1lbnVDb21wb25lbnR7XHJcbiAgICBcclxufSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
