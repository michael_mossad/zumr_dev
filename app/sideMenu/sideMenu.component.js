System.register(["@angular/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SideMenuComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            //import { Router } from '@angular/router-deprecated';
            SideMenuComponent = (function () {
                function SideMenuComponent() {
                }
                SideMenuComponent = __decorate([
                    core_1.Component({
                        selector: "side-menu",
                        templateUrl: "dev/sideMenu/sideMenu.tpl.html"
                    }), 
                    __metadata('design:paramtypes', [])
                ], SideMenuComponent);
                return SideMenuComponent;
            }());
            exports_1("SideMenuComponent", SideMenuComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNpZGVNZW51L3NpZGVNZW51LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQUVBLHNEQUFzRDtZQVF0RDtnQkFBQTtnQkFFQSxDQUFDO2dCQVJEO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFDLFdBQVc7d0JBQ3BCLFdBQVcsRUFBQyxnQ0FBZ0M7cUJBRS9DLENBQUM7O3FDQUFBO2dCQUlGLHdCQUFDO1lBQUQsQ0FGQSxBQUVDLElBQUE7WUFGRCxpREFFQyxDQUFBIiwiZmlsZSI6InNpZGVNZW51L3NpZGVNZW51LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuLy9pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXItZGVwcmVjYXRlZCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOlwic2lkZS1tZW51XCIsXHJcbiAgICB0ZW1wbGF0ZVVybDpcImRldi9zaWRlTWVudS9zaWRlTWVudS50cGwuaHRtbFwiXHJcbi8vICAgIHN0eWxlVXJsczpbXCJhc3NldHMvcGFnZXMvY3NzL2xvZ2luLTUuY3NzXCJdXHJcbn0pXHJcblxyXG5leHBvcnQgIGNsYXNzIFNpZGVNZW51Q29tcG9uZW50e1xyXG4gICAgXHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
