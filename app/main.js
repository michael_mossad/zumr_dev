System.register(['@angular/platform-browser-dynamic', './app.component', './app.routes'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var platform_browser_dynamic_2, app_component_2, app_routes_1;
    return {
        setters:[
            function (platform_browser_dynamic_2_1) {
                platform_browser_dynamic_2 = platform_browser_dynamic_2_1;
            },
            function (app_component_2_1) {
                app_component_2 = app_component_2_1;
            },
            function (app_routes_1_1) {
                app_routes_1 = app_routes_1_1;
            }],
        execute: function() {
            platform_browser_dynamic_2.bootstrap(app_component_2.AppComponent, [
                app_routes_1.APP_ROUTER_PROVIDERS
            ])
                .catch(function (err) { return console.error(err); });
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztZQUlBLG9DQUFTLENBQUMsNEJBQVksRUFBRTtnQkFDdEIsaUNBQW9CO2FBQ3JCLENBQUM7aUJBQ0QsS0FBSyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBib290c3RyYXAgfSAgICAgICAgICAgIGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXItZHluYW1pYyc7XHJcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9ICAgICAgICAgZnJvbSAnLi9hcHAuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQVBQX1JPVVRFUl9QUk9WSURFUlMgfSBmcm9tICcuL2FwcC5yb3V0ZXMnO1xyXG5cclxuYm9vdHN0cmFwKEFwcENvbXBvbmVudCwgW1xyXG4gIEFQUF9ST1VURVJfUFJPVklERVJTXHJcbl0pXHJcbi5jYXRjaChlcnIgPT4gY29uc29sZS5lcnJvcihlcnIpKTsiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
