System.register(["@angular/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var HeaderComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            //import { Router } from '@angular/router-deprecated';
            HeaderComponent = (function () {
                function HeaderComponent() {
                }
                HeaderComponent = __decorate([
                    core_1.Component({
                        selector: "header",
                        templateUrl: "dev/header/header.tpl.html"
                    }), 
                    __metadata('design:paramtypes', [])
                ], HeaderComponent);
                return HeaderComponent;
            }());
            exports_1("HeaderComponent", HeaderComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhlYWRlci9oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBR0Esc0RBQXNEO1lBUXREO2dCQUFBO2dCQVVBLENBQUM7Z0JBaEJEO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFDLFFBQVE7d0JBQ2pCLFdBQVcsRUFBQyw0QkFBNEI7cUJBRTNDLENBQUM7O21DQUFBO2dCQVlGLHNCQUFDO1lBQUQsQ0FWQSxBQVVDLElBQUE7WUFWRCw2Q0FVQyxDQUFBIiwiZmlsZSI6ImhlYWRlci9oZWFkZXIuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5pbXBvcnQge0xvZ2luQ29tcG9uZW50fSBmcm9tIFwiLi4vbG9naW4vbG9naW4uY29tcG9uZW50XCI7XHJcbi8vaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyLWRlcHJlY2F0ZWQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjpcImhlYWRlclwiLFxyXG4gICAgdGVtcGxhdGVVcmw6XCJkZXYvaGVhZGVyL2hlYWRlci50cGwuaHRtbFwiXHJcbi8vICAgIHN0eWxlVXJsczpbXCJhc3NldHMvcGFnZXMvY3NzL2xvZ2luLTUuY3NzXCJdXHJcbn0pXHJcblxyXG5leHBvcnQgIGNsYXNzIEhlYWRlckNvbXBvbmVudHtcclxuLy90ZXN0KExvZ2luQ29tcG9uZW50OmxvZ2luQ29tcG9uZW50KXt9XHJcbi8vICAgIGxvZ091dEFjdGlvbihMb2dpbkNvbXBvbmVudDpsb2dpbkNvbXBvbmVudClcclxuLy8gICAge1xyXG4vLyAgICAgICAgY29uc29sZS5sb2coXCJsb2cgb3V0IGFjdGlvblwiKTsgICAgICAgIFxyXG4vLyAgICAgICAgY29uc29sZS5sb2cobG9naW5Db21wb25lbnQpOyAgICBcclxuLy8gICAgbG9naW5Db21wb25lbnQubG9naW5BY3Rpb24oKTtcclxuLy8gICAgICAgIFxyXG4vLyAgICB9XHJcbiAgICBcclxufSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
