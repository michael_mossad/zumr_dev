System.register(['@angular/router', "./login/login.component", "./testRout/testRout.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, login_component_1, testRout_component_1;
    var AppRoutes, APP_ROUTER_PROVIDERS;
    return {
        setters:[
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (testRout_component_1_1) {
                testRout_component_1 = testRout_component_1_1;
            }],
        execute: function() {
            exports_1("AppRoutes", AppRoutes = [
                {
                    path: '*',
                    component: login_component_1.LoginComponent,
                    useAsDefault: true
                }, {
                    path: '/login',
                    name: 'Login',
                    component: login_component_1.LoginComponent,
                    useAsDefault: true
                },
                {
                    path: '/testRout',
                    name: 'TestRout',
                    component: testRout_component_1.testRoutComponent
                }
            ]);
            ;
            exports_1("APP_ROUTER_PROVIDERS", APP_ROUTER_PROVIDERS = [
                router_1.provideRouter(AppRoutes)
            ]);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztRQUlhLFNBQVMsRUFtQ1Qsb0JBQW9COzs7Ozs7Ozs7Ozs7O1lBbkNwQix1QkFBQSxTQUFTLEdBQUc7Z0JBQ3BCO29CQUNELElBQUksRUFBRSxHQUFHO29CQUNULFNBQVMsRUFBRSxnQ0FBYztvQkFDekIsWUFBWSxFQUFFLElBQUk7aUJBQ25CLEVBQUM7b0JBQ0EsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsSUFBSSxFQUFFLE9BQU87b0JBQ2IsU0FBUyxFQUFFLGdDQUFjO29CQUN6QixZQUFZLEVBQUUsSUFBSTtpQkFDbkI7Z0JBQ0Q7b0JBQ0UsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxVQUFVO29CQUNoQixTQUFTLEVBQUUsc0NBQWlCO2lCQUM3QjthQUNGLENBQUEsQ0FBQztZQUNELENBQUM7WUFrQlcsa0NBQUEsb0JBQW9CLEdBQUc7Z0JBQ2xDLHNCQUFhLENBQUMsU0FBUyxDQUFDO2FBQ3pCLENBQUEsQ0FBQyIsImZpbGUiOiJhcHAucm91dGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgcHJvdmlkZVJvdXRlciwgUm91dGVyQ29uZmlnIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHtMb2dpbkNvbXBvbmVudH0gZnJvbSBcIi4vbG9naW4vbG9naW4uY29tcG9uZW50XCI7XHJcbmltcG9ydCB7dGVzdFJvdXRDb21wb25lbnR9IGZyb20gXCIuL3Rlc3RSb3V0L3Rlc3RSb3V0LmNvbXBvbmVudFwiO1xyXG5cclxuZXhwb3J0IGNvbnN0IEFwcFJvdXRlcyA9IFtcclxuICAgICB7XHJcbiAgICBwYXRoOiAnKicsXHJcbiAgICBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50LFxyXG4gICAgdXNlQXNEZWZhdWx0OiB0cnVlXHJcbiAgfSx7XHJcbiAgICBwYXRoOiAnL2xvZ2luJyxcclxuICAgIG5hbWU6ICdMb2dpbicsXHJcbiAgICBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50LFxyXG4gICAgdXNlQXNEZWZhdWx0OiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICBwYXRoOiAnL3Rlc3RSb3V0JyxcclxuICAgIG5hbWU6ICdUZXN0Um91dCcsXHJcbiAgICBjb21wb25lbnQ6IHRlc3RSb3V0Q29tcG9uZW50XHJcbiAgfSBcclxuXSlcclxuXTtcclxuQFJvdXRlcyhbXHJcbiAgICB7XHJcbiAgICBwYXRoOiAnKicsXHJcbiAgICBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50LFxyXG4gICAgdXNlQXNEZWZhdWx0OiB0cnVlXHJcbiAgfSx7XHJcbiAgICBwYXRoOiAnL2xvZ2luJyxcclxuICAgIG5hbWU6ICdMb2dpbicsXHJcbiAgICBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50LFxyXG4gICAgdXNlQXNEZWZhdWx0OiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICBwYXRoOiAnL3Rlc3RSb3V0JyxcclxuICAgIG5hbWU6ICdUZXN0Um91dCcsXHJcbiAgICBjb21wb25lbnQ6IHRlc3RSb3V0Q29tcG9uZW50XHJcbiAgfSBcclxuXSlcclxuZXhwb3J0IGNvbnN0IEFQUF9ST1VURVJfUFJPVklERVJTID0gW1xyXG4gIHByb3ZpZGVSb3V0ZXIoQXBwUm91dGVzKVxyXG5dOyJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
