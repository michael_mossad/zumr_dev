System.register(["./login/login.component", "./testRout/testRout.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var login_component_1, testRout_component_1;
    var AppRoutes, APP_ROUTER_PROVIDERS, default_1;
    return {
        setters:[
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (testRout_component_1_1) {
                testRout_component_1 = testRout_component_1_1;
            }],
        execute: function() {
            //import {ROUTER_DIRECTIVES,ROUTER_PROVIDERS} from '@angular/router';
            //import {ProfileComponent} from "./profile/profile.component";
            exports_1("AppRoutes", AppRoutes = [
                {
                    path: '*',
                    component: login_component_1.LoginComponent,
                    useAsDefault: true
                }, {
                    path: '/login',
                    name: 'Login',
                    component: login_component_1.LoginComponent,
                    useAsDefault: true
                },
                {
                    path: '/testRout',
                    name: 'TestRout',
                    component: testRout_component_1.testRoutComponent
                }
            ]);
            ;
            exports_1("APP_ROUTER_PROVIDERS", APP_ROUTER_PROVIDERS = [
                provideRouter(AppRoutes)
            ]);
            default_1 = (function () {
                function default_1(router) {
                    this.router = router;
                }
                default_1.prototype.ngOnInit = function () {
                    this.router.navigate(['/login']);
                };
                return default_1;
            }());
            exports_1("", default_1);
        }
    }
});
// @RouteConfig([
//     { path: '/login',  name: 'Login',  component: LoginComponent, useAsDefault: true },
//     { path: '/profile', name: 'Profile', component: ProfileComponent }
// ])
//export const AppRoutes = [
//  {
//    path: '/login',
//    name: 'Login',
//    component: LoginComponent,
//    useAsDefault: true
//  },
//  {
//    path: '/testRout',
//    name: 'TestRout',
//    component: testRoutComponent
//  }
//];
//@RouteConfig([
//  {
//    path: '/login',
//    name: 'Login',
//    component: LoginComponent,
//    useAsDefault: true
//  },
//  {
//    path: '/testRout',
//    name: 'TestRout',
//    component: testRoutComponent
//  }
//])

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQgLSBDb3B5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7UUEyQmEsU0FBUyxFQW1DVCxvQkFBb0I7Ozs7Ozs7Ozs7WUF4RGpDLHFFQUFxRTtZQUVyRSwrREFBK0Q7WUFtQmxELHVCQUFBLFNBQVMsR0FBRztnQkFDcEI7b0JBQ0QsSUFBSSxFQUFFLEdBQUc7b0JBQ1QsU0FBUyxFQUFFLGdDQUFjO29CQUN6QixZQUFZLEVBQUUsSUFBSTtpQkFDbkIsRUFBQztvQkFDQSxJQUFJLEVBQUUsUUFBUTtvQkFDZCxJQUFJLEVBQUUsT0FBTztvQkFDYixTQUFTLEVBQUUsZ0NBQWM7b0JBQ3pCLFlBQVksRUFBRSxJQUFJO2lCQUNuQjtnQkFDRDtvQkFDRSxJQUFJLEVBQUUsV0FBVztvQkFDakIsSUFBSSxFQUFFLFVBQVU7b0JBQ2hCLFNBQVMsRUFBRSxzQ0FBaUI7aUJBQzdCO2FBQ0YsQ0FBQSxDQUFDO1lBQ0QsQ0FBQztZQWtCVyxrQ0FBQSxvQkFBb0IsR0FBRztnQkFDbEMsYUFBYSxDQUFDLFNBQVMsQ0FBQzthQUN6QixDQUFBLENBQUM7WUFDRjtnQkFFSSxtQkFBb0IsTUFBYztvQkFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO2dCQUFHLENBQUM7Z0JBRXRDLDRCQUFRLEdBQVI7b0JBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDO2dCQUNMLGdCQUFDO1lBQUQsQ0FQQSxBQU9DLElBQUE7WUFQRCx3QkFPQyxDQUFBOzs7O0FBRUQsaUJBQWlCO0FBQ2pCLDBGQUEwRjtBQUMxRix5RUFBeUU7QUFDekUsS0FBSztBQUVMLDRCQUE0QjtBQUM1QixLQUFLO0FBQ0wscUJBQXFCO0FBQ3JCLG9CQUFvQjtBQUNwQixnQ0FBZ0M7QUFDaEMsd0JBQXdCO0FBQ3hCLE1BQU07QUFDTixLQUFLO0FBQ0wsd0JBQXdCO0FBQ3hCLHVCQUF1QjtBQUN2QixrQ0FBa0M7QUFDbEMsS0FBSztBQUNMLElBQUk7QUFFSixnQkFBZ0I7QUFDaEIsS0FBSztBQUNMLHFCQUFxQjtBQUNyQixvQkFBb0I7QUFDcEIsZ0NBQWdDO0FBQ2hDLHdCQUF3QjtBQUN4QixNQUFNO0FBQ04sS0FBSztBQUNMLHdCQUF3QjtBQUN4Qix1QkFBdUI7QUFDdkIsa0NBQWtDO0FBQ2xDLEtBQUs7QUFDTCxJQUFJIiwiZmlsZSI6ImFwcC5jb21wb25lbnQgLSBDb3B5LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8vPHJlZmVyZW5jZSBwYXRoPVwiLi4vbm9kZV9tb2R1bGVzL0Bhbmd1bGFyL3JvdXRlci1kZXByZWNhdGVkL3JvdXRlci5kLnRzXCIvPlxyXG5pbXBvcnQge0NvbXBvbmVudCxPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgUm91dGVzLCBSb3V0ZXIsIFJPVVRFUl9ESVJFQ1RJVkVTLCBST1VURVJfUFJPVklERVJTfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQge0xvZ2luQ29tcG9uZW50fSBmcm9tIFwiLi9sb2dpbi9sb2dpbi5jb21wb25lbnRcIjtcclxuaW1wb3J0IHt0ZXN0Um91dENvbXBvbmVudH0gZnJvbSBcIi4vdGVzdFJvdXQvdGVzdFJvdXQuY29tcG9uZW50XCI7XHJcbi8vaW1wb3J0IHtST1VURVJfRElSRUNUSVZFUyxST1VURVJfUFJPVklERVJTfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuLy9pbXBvcnQge1Byb2ZpbGVDb21wb25lbnR9IGZyb20gXCIuL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnRcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdteS1hcHAnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuXHJcbiAgICA8aDEgY2xhc3M9XCJ0aXRsZVwiPkNvbXBvbmVudCBSb3V0ZXI8L2gxPlxyXG4gICAgPG5hdj5cclxuICAgIDxhIFtyb3V0ZXJMaW5rXT1cIlsnL3Rlc3RSb3V0J11cIj50ZXN0Um91dGU8L2E+XHJcbiAgICA8YSBbcm91dGVyTGlua109XCJbJy9sb2dpbiddXCI+TG9naW48L2E+XHJcbiAgICA8L25hdj5cclxuICAgIDxyb3V0ZXItb3V0bGV0Pjwvcm91dGVyLW91dGxldD5cclxuXHJcbiAgICBgLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICBST1VURVJfUFJPVklERVJTXSxcclxuICAgIGRpcmVjdGl2ZXM6IFtST1VURVJfRElSRUNUSVZFUyxMb2dpbkNvbXBvbmVudCx0ZXN0Um91dENvbXBvbmVudF1cclxuICAgIH0pXHJcblxyXG5leHBvcnQgY29uc3QgQXBwUm91dGVzID0gW1xyXG4gICAgIHtcclxuICAgIHBhdGg6ICcqJyxcclxuICAgIGNvbXBvbmVudDogTG9naW5Db21wb25lbnQsXHJcbiAgICB1c2VBc0RlZmF1bHQ6IHRydWVcclxuICB9LHtcclxuICAgIHBhdGg6ICcvbG9naW4nLFxyXG4gICAgbmFtZTogJ0xvZ2luJyxcclxuICAgIGNvbXBvbmVudDogTG9naW5Db21wb25lbnQsXHJcbiAgICB1c2VBc0RlZmF1bHQ6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHBhdGg6ICcvdGVzdFJvdXQnLFxyXG4gICAgbmFtZTogJ1Rlc3RSb3V0JyxcclxuICAgIGNvbXBvbmVudDogdGVzdFJvdXRDb21wb25lbnRcclxuICB9IFxyXG5dKVxyXG5dO1xyXG5AUm91dGVzKFtcclxuICAgIHtcclxuICAgIHBhdGg6ICcqJyxcclxuICAgIGNvbXBvbmVudDogTG9naW5Db21wb25lbnQsXHJcbiAgICB1c2VBc0RlZmF1bHQ6IHRydWVcclxuICB9LHtcclxuICAgIHBhdGg6ICcvbG9naW4nLFxyXG4gICAgbmFtZTogJ0xvZ2luJyxcclxuICAgIGNvbXBvbmVudDogTG9naW5Db21wb25lbnQsXHJcbiAgICB1c2VBc0RlZmF1bHQ6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHBhdGg6ICcvdGVzdFJvdXQnLFxyXG4gICAgbmFtZTogJ1Rlc3RSb3V0JyxcclxuICAgIGNvbXBvbmVudDogdGVzdFJvdXRDb21wb25lbnRcclxuICB9IFxyXG5dKVxyXG5leHBvcnQgY29uc3QgQVBQX1JPVVRFUl9QUk9WSURFUlMgPSBbXHJcbiAgcHJvdmlkZVJvdXRlcihBcHBSb3V0ZXMpXHJcbl07XHJcbmV4cG9ydCBjbGFzcyBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge31cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcclxuICAgIH1cclxufVxyXG5cclxuLy8gQFJvdXRlQ29uZmlnKFtcclxuLy8gICAgIHsgcGF0aDogJy9sb2dpbicsICBuYW1lOiAnTG9naW4nLCAgY29tcG9uZW50OiBMb2dpbkNvbXBvbmVudCwgdXNlQXNEZWZhdWx0OiB0cnVlIH0sXHJcbi8vICAgICB7IHBhdGg6ICcvcHJvZmlsZScsIG5hbWU6ICdQcm9maWxlJywgY29tcG9uZW50OiBQcm9maWxlQ29tcG9uZW50IH1cclxuLy8gXSlcclxuXHJcbi8vZXhwb3J0IGNvbnN0IEFwcFJvdXRlcyA9IFtcclxuLy8gIHtcclxuLy8gICAgcGF0aDogJy9sb2dpbicsXHJcbi8vICAgIG5hbWU6ICdMb2dpbicsXHJcbi8vICAgIGNvbXBvbmVudDogTG9naW5Db21wb25lbnQsXHJcbi8vICAgIHVzZUFzRGVmYXVsdDogdHJ1ZVxyXG4vLyAgfSxcclxuLy8gIHtcclxuLy8gICAgcGF0aDogJy90ZXN0Um91dCcsXHJcbi8vICAgIG5hbWU6ICdUZXN0Um91dCcsXHJcbi8vICAgIGNvbXBvbmVudDogdGVzdFJvdXRDb21wb25lbnRcclxuLy8gIH1cclxuLy9dO1xyXG5cclxuLy9AUm91dGVDb25maWcoW1xyXG4vLyAge1xyXG4vLyAgICBwYXRoOiAnL2xvZ2luJyxcclxuLy8gICAgbmFtZTogJ0xvZ2luJyxcclxuLy8gICAgY29tcG9uZW50OiBMb2dpbkNvbXBvbmVudCxcclxuLy8gICAgdXNlQXNEZWZhdWx0OiB0cnVlXHJcbi8vICB9LFxyXG4vLyAge1xyXG4vLyAgICBwYXRoOiAnL3Rlc3RSb3V0JyxcclxuLy8gICAgbmFtZTogJ1Rlc3RSb3V0JyxcclxuLy8gICAgY29tcG9uZW50OiB0ZXN0Um91dENvbXBvbmVudFxyXG4vLyAgfVxyXG4vL10pXHJcblxyXG5cclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
