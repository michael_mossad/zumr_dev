System.register(["@angular/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var testRoutComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            testRoutComponent = (function () {
                function testRoutComponent() {
                }
                testRoutComponent = __decorate([
                    core_1.Component({
                        selector: "testRout",
                        templateUrl: "dev/testRout/testRout.tpl.html"
                    }), 
                    __metadata('design:paramtypes', [])
                ], testRoutComponent);
                return testRoutComponent;
            }());
            exports_1("testRoutComponent", testRoutComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlc3RSb3V0L3Rlc3RSb3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQVFBO2dCQUFBO2dCQUVBLENBQUM7Z0JBUEQ7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDUCxRQUFRLEVBQUMsVUFBVTt3QkFDbkIsV0FBVyxFQUFDLGdDQUFnQztxQkFDL0MsQ0FBQzs7cUNBQUE7Z0JBSUYsd0JBQUM7WUFBRCxDQUZBLEFBRUMsSUFBQTtZQUZELGlEQUVDLENBQUEiLCJmaWxlIjoidGVzdFJvdXQvdGVzdFJvdXQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjpcInRlc3RSb3V0XCIsXHJcbiAgICB0ZW1wbGF0ZVVybDpcImRldi90ZXN0Um91dC90ZXN0Um91dC50cGwuaHRtbFwiXHJcbn0pXHJcblxyXG5leHBvcnQgIGNsYXNzIHRlc3RSb3V0Q29tcG9uZW50e1xyXG4gIFxyXG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
