System.register(['@angular/platform-browser-dynamic', "./app.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var platform_browser_dynamic_2, app_component_2;
    return {
        setters:[
            function (platform_browser_dynamic_2_1) {
                platform_browser_dynamic_2 = platform_browser_dynamic_2_1;
            },
            function (app_component_2_1) {
                app_component_2 = app_component_2_1;
            }],
        execute: function() {
            platform_browser_dynamic_2.bootstrap(app_component_2.AppComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvb3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztZQUlBLG9DQUFTLENBQUMsNEJBQVksQ0FBQyxDQUFBIiwiZmlsZSI6ImJvb3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge2Jvb3RzdHJhcH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci1keW5hbWljJztcclxuaW1wb3J0IHtBcHBDb21wb25lbnR9IGZyb20gXCIuL2FwcC5jb21wb25lbnRcIjtcclxuXHJcblxyXG5ib290c3RyYXAoQXBwQ29tcG9uZW50KSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
