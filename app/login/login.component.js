System.register(["@angular/core", "../header/header.component", "../sideMenu/sideMenu.component", "../footer/footer.component", '../globals'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, header_component_1, sideMenu_component_1, footer_component_1, myGlobals;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (header_component_1_1) {
                header_component_1 = header_component_1_1;
            },
            function (sideMenu_component_1_1) {
                sideMenu_component_1 = sideMenu_component_1_1;
            },
            function (footer_component_1_1) {
                footer_component_1 = footer_component_1_1;
            },
            function (myGlobals_1) {
                myGlobals = myGlobals_1;
            }],
        execute: function() {
            //import {AppComponent} from '../app.component';
            //import { Router } from '@angular/router-deprecated';
            LoginComponent = (function () {
                function LoginComponent() {
                    ////    passwordStrength=0;
                    this.workTemp = {};
                    this.workList = [
                        {
                            work: "work1",
                            category: "category1"
                        },
                        {
                            work: "work2",
                            category: "category2"
                        }
                    ];
                    this.registerFormIsValid = false;
                    this.templateVisibility = false;
                    this.confirmPassword = "";
                    this.emailAddressValidation = '';
                    this.emailAddress = "";
                    this.emailAddressConfirmation = "";
                    this.password = "";
                    this.forgetPass = true;
                    this.signup = true;
                    this.login = false;
                    this.resetPass = true;
                    this.signupConfirm = true;
                }
                LoginComponent.prototype.addNewWork = function () {
                    if (this.workTemp.category === '1')
                        this.workTemp.category = "Category 1";
                    if (this.workTemp.category === '2')
                        this.workTemp.category = "Category 2";
                    this.workList.push(this.workTemp);
                    this.workTemp = {};
                };
                LoginComponent.prototype.ngOnInit = function () {
                    console.log("-----------------------");
                    console.log("myGlobals");
                    console.log(myGlobals.templateVisibility);
                    this.templateVisibility = myGlobals.templateVisibility;
                    myGlobals.templateVisibility;
                };
                //let router = Router;
                //    constructor(
                //    private router: Router,app:AppComponent) {
                //    console.log(app);
                //  }
                //    console.log(2);
                //    console.log("-----------------------");
                LoginComponent.prototype.checkFormValidation = function () {
                    if (this.emailAddressValidation === 1
                        && this.confirmPassword === this.password
                        && this.username != ''
                        && this.displayname != ''
                        && this.emailAddressConfirmation === this.emailAddress) {
                        return true;
                    }
                    else {
                        this.confirmPassword = null;
                        this.emailAddressValidation = 0;
                        this.emailAddressConfirmation = " ";
                        return false;
                    }
                };
                LoginComponent.prototype.checkMailForamte = function (val) {
                    if (/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/.test(this.emailAddress))
                        this.emailAddressValidation = 1;
                    else
                        this.emailAddressValidation = 0;
                };
                LoginComponent.prototype.checkPass = function (val) {
                    console.log('at the changes check');
                    if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(this.password)) {
                        console.log(2);
                        this.passwordStrength = 2;
                    }
                    else if (/^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/.test(this.password)) {
                        console.log(1);
                        this.passwordStrength = 1;
                    }
                    else {
                        console.log(0);
                        this.passwordStrength = 0;
                    }
                };
                LoginComponent.prototype.showForgetPassword = function () {
                    this.forgetPass = false;
                    this.signup = true;
                    this.login = true;
                };
                LoginComponent.prototype.showSignup = function () {
                    this.forgetPass = true;
                    this.signup = false;
                    this.login = true;
                };
                LoginComponent.prototype.showLogin = function () {
                    this.forgetPass = true;
                    this.signup = true;
                    this.login = false;
                    this.forgetPass = true;
                    this.resetPass = true;
                    this.signupConfirm = true;
                };
                LoginComponent.prototype.signupConfirmAction = function () {
                    if (this.checkFormValidation()) {
                        this.signup = true;
                        this.signupConfirm = false;
                    }
                };
                LoginComponent.prototype.resetPassAction = function () {
                    if (/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/.test(this.resetEmail)) {
                        this.forgetPass = true;
                        this.resetPass = false;
                    }
                    else
                        this.resetEmail = "";
                };
                LoginComponent.prototype.loginAction = function () {
                    console.log(this.loginPass);
                    console.log(this.loginMail);
                    console.log(this.loginMail != "");
                    if (this.loginPass && this.loginMail) {
                        this.templateVisibility = true;
                        myGlobals.templateVisibility = true;
                    }
                    //         let link = ['TestRout'];
                    //         this.router.navigate(link);
                };
                LoginComponent.prototype.logOutAction = function () {
                    this.templateVisibility = false;
                    //        console.log("log out action");        
                    //        console.log(this.loginComponent);    
                    //    this.loginComponent.loginAction();
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        selector: "login",
                        templateUrl: "dev/login/login.tpl.html",
                        styleUrls: ["assets/pages/css/login-5.css"],
                        directives: [header_component_1.HeaderComponent, sideMenu_component_1.SideMenuComponent, footer_component_1.FooterComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_1("LoginComponent", LoginComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQU1BLGdEQUFnRDtZQUVoRCxzREFBc0Q7WUFTdEQ7Z0JBQUE7b0JBQ0EsMkJBQTJCO29CQUN2QixhQUFRLEdBQUksRUFDUCxDQUFBO29CQUNMLGFBQVEsR0FBRzt3QkFDUDs0QkFDSSxJQUFJLEVBQUcsT0FBTzs0QkFDZCxRQUFRLEVBQUcsV0FBVzt5QkFDekI7d0JBQ0Q7NEJBQ0ksSUFBSSxFQUFHLE9BQU87NEJBQ2QsUUFBUSxFQUFHLFdBQVc7eUJBQ3pCO3FCQUVKLENBQUE7b0JBV0Qsd0JBQW1CLEdBQUcsS0FBSyxDQUFDO29CQUM1Qix1QkFBa0IsR0FBRyxLQUFLLENBQUM7b0JBQzNCLG9CQUFlLEdBQUcsRUFBRSxDQUFDO29CQUNyQiwyQkFBc0IsR0FBRyxFQUFFLENBQUM7b0JBQzVCLGlCQUFZLEdBQUcsRUFBRSxDQUFDO29CQUNsQiw2QkFBd0IsR0FBRyxFQUFFLENBQUM7b0JBQzlCLGFBQVEsR0FBRyxFQUFFLENBQUM7b0JBQ2QsZUFBVSxHQUFHLElBQUksQ0FBQztvQkFDbEIsV0FBTSxHQUFHLElBQUksQ0FBQztvQkFDZCxVQUFLLEdBQUcsS0FBSyxDQUFDO29CQUNkLGNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ2pCLGtCQUFhLEdBQUUsSUFBSSxDQUFDO2dCQTJIeEIsQ0FBQztnQkFoSkcsbUNBQVUsR0FBVjtvQkFFUSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsS0FBSSxHQUFHLENBQUM7d0JBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQztvQkFDMUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEtBQUksR0FBRyxDQUFDO3dCQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUM7b0JBQzlDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDbEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLENBQUM7Z0JBaUJELGlDQUFRLEdBQVI7b0JBRUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFBO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUN0QyxJQUFJLENBQUMsa0JBQWtCLEdBQUUsU0FBUyxDQUFDLGtCQUFrQixDQUFDO29CQUMxRCxTQUFTLENBQUMsa0JBQWtCLENBQUM7Z0JBQzdCLENBQUM7Z0JBR0Qsc0JBQXNCO2dCQUUxQixrQkFBa0I7Z0JBQ2xCLGdEQUFnRDtnQkFDaEQsdUJBQXVCO2dCQUN2QixLQUFLO2dCQUNMLHFCQUFxQjtnQkFDckIsNkNBQTZDO2dCQUN6Qyw0Q0FBbUIsR0FBbkI7b0JBRVEsRUFBRSxDQUFBLENBQUUsSUFBSSxDQUFDLHNCQUFzQixLQUFLLENBQUM7MkJBQzlCLElBQUksQ0FBQyxlQUFlLEtBQUssSUFBSSxDQUFDLFFBQVE7MkJBQ3RDLElBQUksQ0FBQyxRQUFRLElBQUksRUFBRTsyQkFDbkIsSUFBSSxDQUFDLFdBQVcsSUFBSSxFQUFFOzJCQUN0QixJQUFJLENBQUMsd0JBQXdCLEtBQU0sSUFBSSxDQUFDLFlBQy9DLENBQUMsQ0FDRCxDQUFDO3dCQUNHLE1BQU0sQ0FBQyxJQUFJLENBQUM7b0JBQ2hCLENBQUM7b0JBQ0QsSUFBSSxDQUFBLENBQUM7d0JBQ0QsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7d0JBQzVCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxDQUFDLENBQUM7d0JBQ2hDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxHQUFHLENBQUM7d0JBQ3BDLE1BQU0sQ0FBQyxLQUFLLENBQUM7b0JBQ2pCLENBQUM7Z0JBR1QsQ0FBQztnQkFDRCx5Q0FBZ0IsR0FBaEIsVUFBa0IsR0FBRztvQkFFYixFQUFFLENBQUEsQ0FBQyw4Q0FBOEMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUN0RSxJQUFJLENBQUMsc0JBQXNCLEdBQUcsQ0FBQyxDQUFDO29CQUNwQyxJQUFJO3dCQUNBLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxDQUFDLENBQUM7Z0JBQzVDLENBQUM7Z0JBQ0Qsa0NBQVMsR0FBVCxVQUFVLEdBQUc7b0JBRVosT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO29CQUNqQyxFQUFFLENBQUEsQ0FBQyxnRUFBZ0UsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FDdEYsQ0FBQyxDQUFELENBQUM7d0JBQ0EsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDWixJQUFJLENBQUMsZ0JBQWdCLEdBQUUsQ0FBQyxDQUFDO29CQUM3QixDQUFDO29CQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyx3RkFBd0YsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQ3RILENBQUM7d0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDWCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO29CQUM5QixDQUFDO29CQUNGLElBQUksQ0FBQSxDQUFDO3dCQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ1gsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQztvQkFDL0IsQ0FBQztnQkFDSixDQUFDO2dCQUNELDJDQUFrQixHQUFsQjtvQkFDSSxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUN0QixDQUFDO2dCQUNELG1DQUFVLEdBQVY7b0JBQ0ksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztnQkFDdEIsQ0FBQztnQkFDRCxrQ0FBUyxHQUFUO29CQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29CQUN2QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztvQkFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7b0JBQ25CLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29CQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDdEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7Z0JBQzlCLENBQUM7Z0JBQ0QsNENBQW1CLEdBQW5CO29CQUNRLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQzlCLENBQUM7d0JBQ0csSUFBSSxDQUFDLE1BQU0sR0FBQyxJQUFJLENBQUM7d0JBQ2pCLElBQUksQ0FBQyxhQUFhLEdBQUMsS0FBSyxDQUFDO29CQUM3QixDQUFDO2dCQUNULENBQUM7Z0JBQ0Qsd0NBQWUsR0FBZjtvQkFDUSxFQUFFLENBQUEsQ0FBQyw4Q0FBOEMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQ3hFLENBQUM7d0JBQ0csSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7d0JBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUMzQixDQUFDO29CQUNELElBQUk7d0JBQ0EsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ2pDLENBQUM7Z0JBQ0Qsb0NBQVcsR0FBWDtvQkFFQSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQztvQkFDMUIsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQ3BDLENBQUM7d0JBQ0csSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQzt3QkFDL0IsU0FBUyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztvQkFDeEMsQ0FBQztvQkFFYixtQ0FBbUM7b0JBQ25DLHNDQUFzQztnQkFDbEMsQ0FBQztnQkFDRCxxQ0FBWSxHQUFaO29CQUVJLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7b0JBQ3hDLGdEQUFnRDtvQkFDaEQsK0NBQStDO29CQUMvQyx3Q0FBd0M7Z0JBRXBDLENBQUM7Z0JBcktMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFDLE9BQU87d0JBQ2hCLFdBQVcsRUFBQywwQkFBMEI7d0JBQ3RDLFNBQVMsRUFBQyxDQUFDLDhCQUE4QixDQUFDO3dCQUMxQyxVQUFVLEVBQUUsQ0FBQyxrQ0FBZSxFQUFDLHNDQUFpQixFQUFDLGtDQUFlLENBQUM7cUJBQ2xFLENBQUM7O2tDQUFBO2dCQWlLRixxQkFBQztZQUFELENBL0pBLEFBK0pDLElBQUE7WUEvSkQsMkNBK0pDLENBQUEiLCJmaWxlIjoibG9naW4vbG9naW4uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsT25Jbml0fSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuaW1wb3J0IHtIZWFkZXJDb21wb25lbnR9IGZyb20gXCIuLi9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge1NpZGVNZW51Q29tcG9uZW50fSBmcm9tIFwiLi4vc2lkZU1lbnUvc2lkZU1lbnUuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7Rm9vdGVyQ29tcG9uZW50fSBmcm9tIFwiLi4vZm9vdGVyL2Zvb3Rlci5jb21wb25lbnRcIjtcclxuaW1wb3J0IG15R2xvYmFscyA9IHJlcXVpcmUoJy4uL2dsb2JhbHMnKTtcclxuLy9pbXBvcnQge0FwcENvbXBvbmVudH0gZnJvbSAnLi4vYXBwLmNvbXBvbmVudCc7XHJcblxyXG4vL2ltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlci1kZXByZWNhdGVkJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6XCJsb2dpblwiLFxyXG4gICAgdGVtcGxhdGVVcmw6XCJkZXYvbG9naW4vbG9naW4udHBsLmh0bWxcIixcclxuICAgIHN0eWxlVXJsczpbXCJhc3NldHMvcGFnZXMvY3NzL2xvZ2luLTUuY3NzXCJdLFxyXG4gICAgZGlyZWN0aXZlczogW0hlYWRlckNvbXBvbmVudCxTaWRlTWVudUNvbXBvbmVudCxGb290ZXJDb21wb25lbnRdXHJcbn0pXHJcblxyXG5leHBvcnQgIGNsYXNzIExvZ2luQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0e1xyXG4vLy8vICAgIHBhc3N3b3JkU3RyZW5ndGg9MDtcclxuICAgIHdvcmtUZW1wID0gIHtcclxuICAgICAgICB9XHJcbiAgICB3b3JrTGlzdCA9IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHdvcmsgOiBcIndvcmsxXCIsXHJcbiAgICAgICAgICAgIGNhdGVnb3J5IDogXCJjYXRlZ29yeTFcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB3b3JrIDogXCJ3b3JrMlwiLFxyXG4gICAgICAgICAgICBjYXRlZ29yeSA6IFwiY2F0ZWdvcnkyXCJcclxuICAgICAgICB9XHJcblxyXG4gICAgXVxyXG4gICAgYWRkTmV3V29yaygpXHJcbiAgICB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLndvcmtUZW1wLmNhdGVnb3J5ID09PScxJylcclxuICAgICAgICAgICAgICAgIHRoaXMud29ya1RlbXAuY2F0ZWdvcnkgPSBcIkNhdGVnb3J5IDFcIjtcclxuICAgICAgICAgICAgaWYgKHRoaXMud29ya1RlbXAuY2F0ZWdvcnkgPT09JzInKVxyXG4gICAgICAgICAgICAgICAgdGhpcy53b3JrVGVtcC5jYXRlZ29yeSA9IFwiQ2F0ZWdvcnkgMlwiO1xyXG4gICAgICAgIHRoaXMud29ya0xpc3QucHVzaCh0aGlzLndvcmtUZW1wKTsgICBcclxuICAgICAgICB0aGlzLndvcmtUZW1wID0ge307XHJcbiAgICB9XHJcblxyXG4gICAgcmVnaXN0ZXJGb3JtSXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgdGVtcGxhdGVWaXNpYmlsaXR5ID0gZmFsc2U7XHJcbiAgICBjb25maXJtUGFzc3dvcmQgPSBcIlwiO1xyXG4gICAgZW1haWxBZGRyZXNzVmFsaWRhdGlvbiA9ICcnO1xyXG4gICAgZW1haWxBZGRyZXNzID0gXCJcIjtcclxuICAgIGVtYWlsQWRkcmVzc0NvbmZpcm1hdGlvbiA9IFwiXCI7XHJcbiAgICBwYXNzd29yZCA9IFwiXCI7XHJcbiAgICBmb3JnZXRQYXNzID0gdHJ1ZTtcclxuICAgIHNpZ251cCA9IHRydWU7XHJcbiAgICBsb2dpbiA9IGZhbHNlO1xyXG4gICAgcmVzZXRQYXNzID0gdHJ1ZTtcclxuICAgIHNpZ251cENvbmZpcm09IHRydWU7XHJcbiAgICBcclxuICAgIFxyXG4gICAgIFxyXG4gICAgbmdPbkluaXQoKVxyXG4gICAge1xyXG4gICAgICBjb25zb2xlLmxvZyhcIi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXCIpICBcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJteUdsb2JhbHNcIilcclxuICAgIGNvbnNvbGUubG9nKG15R2xvYmFscy50ZW1wbGF0ZVZpc2liaWxpdHkpO1xyXG4gICAgICAgIHRoaXMudGVtcGxhdGVWaXNpYmlsaXR5PSBteUdsb2JhbHMudGVtcGxhdGVWaXNpYmlsaXR5O1xyXG4gICAgbXlHbG9iYWxzLnRlbXBsYXRlVmlzaWJpbGl0eTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgXHJcbiAgICAvL2xldCByb3V0ZXIgPSBSb3V0ZXI7XHJcbiAgICBcclxuLy8gICAgY29uc3RydWN0b3IoXHJcbi8vICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsYXBwOkFwcENvbXBvbmVudCkge1xyXG4vLyAgICBjb25zb2xlLmxvZyhhcHApO1xyXG4vLyAgfVxyXG4vLyAgICBjb25zb2xlLmxvZygyKTtcclxuLy8gICAgY29uc29sZS5sb2coXCItLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVwiKTtcclxuICAgIGNoZWNrRm9ybVZhbGlkYXRpb24oKVxyXG4gICAge1xyXG4gICAgICAgICAgICBpZiggdGhpcy5lbWFpbEFkZHJlc3NWYWxpZGF0aW9uID09PSAxIFxyXG4gICAgICAgICAgICAgICAgJiYgdGhpcy5jb25maXJtUGFzc3dvcmQgPT09IHRoaXMucGFzc3dvcmRcclxuICAgICAgICAgICAgICAgICYmIHRoaXMudXNlcm5hbWUgIT0gJydcclxuICAgICAgICAgICAgICAgICYmIHRoaXMuZGlzcGxheW5hbWUgIT0gJydcclxuICAgICAgICAgICAgICAgICYmIHRoaXMuZW1haWxBZGRyZXNzQ29uZmlybWF0aW9uID09PSAgdGhpcy5lbWFpbEFkZHJlc3NcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb25maXJtUGFzc3dvcmQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWFpbEFkZHJlc3NWYWxpZGF0aW9uID0gMDtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW1haWxBZGRyZXNzQ29uZmlybWF0aW9uID0gXCIgXCI7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBcclxuICAgIH1cclxuICAgIGNoZWNrTWFpbEZvcmFtdGUgKHZhbClcclxuICAgIHtcclxuICAgICAgICAgICAgaWYoL15cXHcrW1xcdy1cXC5dKlxcQFxcdysoKC1cXHcrKXwoXFx3KikpXFwuW2Etel17MiwzfSQvLnRlc3QodGhpcy5lbWFpbEFkZHJlc3MpKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWFpbEFkZHJlc3NWYWxpZGF0aW9uID0gMTtcclxuICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWFpbEFkZHJlc3NWYWxpZGF0aW9uID0gMDtcclxuICAgIH1cclxuICAgIGNoZWNrUGFzcyh2YWwpXHJcbiAgICB7XHJcbiAgICAgY29uc29sZS5sb2coJ2F0IHRoZSBjaGFuZ2VzIGNoZWNrJyk7XHJcbiAgICAgICAgaWYoL14oPz0uKlthLXpdKSg/PS4qW0EtWl0pKD89LipbMC05XSkoPz0uKlshQCNcXCQlXFxeJlxcKl0pKD89Lns4LH0pLy50ZXN0KHRoaXMucGFzc3dvcmQpXHJcbiAgICAgICAge1xyXG4gICAgICAgICBjb25zb2xlLmxvZygyKTtcclxuICAgICAgICAgICAgdGhpcy5wYXNzd29yZFN0cmVuZ3RoPSAyO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmICgvXigoKD89LipbYS16XSkoPz0uKltBLVpdKSl8KCg/PS4qW2Etel0pKD89LipbMC05XSkpfCgoPz0uKltBLVpdKSg/PS4qWzAtOV0pKSkoPz0uezYsfSkvLnRlc3QodGhpcy5wYXNzd29yZCkpXHJcbiAgICAgICAge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKDEpO1xyXG4gICAgICAgICAgICB0aGlzLnBhc3N3b3JkU3RyZW5ndGggPSAxO1xyXG4gICAgICAgIH1cclxuICAgICAgIGVsc2V7XHJcbiAgICAgICAgY29uc29sZS5sb2coMCk7XHJcbiAgICAgICAgICAgIHRoaXMucGFzc3dvcmRTdHJlbmd0aCA9IDA7XHJcbiAgICAgICB9XHJcbiAgICB9XHJcbiAgICBzaG93Rm9yZ2V0UGFzc3dvcmQoKXtcclxuICAgICAgICB0aGlzLmZvcmdldFBhc3MgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnNpZ251cCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5sb2dpbiA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBzaG93U2lnbnVwKCl7XHJcbiAgICAgICAgdGhpcy5mb3JnZXRQYXNzID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnNpZ251cCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMubG9naW4gPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgc2hvd0xvZ2luKCl7XHJcbiAgICAgICAgdGhpcy5mb3JnZXRQYXNzID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnNpZ251cCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5sb2dpbiA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZm9yZ2V0UGFzcyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5yZXNldFBhc3MgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuc2lnbnVwQ29uZmlybSA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBzaWdudXBDb25maXJtQWN0aW9uKCl7XHJcbiAgICAgICAgICAgIGlmKHRoaXMuY2hlY2tGb3JtVmFsaWRhdGlvbigpKVxyXG4gICAgICAgICAgICB7ICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zaWdudXA9dHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2lnbnVwQ29uZmlybT1mYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmVzZXRQYXNzQWN0aW9uKCl7XHJcbiAgICAgICAgICAgIGlmKC9eXFx3K1tcXHctXFwuXSpcXEBcXHcrKCgtXFx3Kyl8KFxcdyopKVxcLlthLXpdezIsM30kLy50ZXN0KHRoaXMucmVzZXRFbWFpbCkpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9yZ2V0UGFzcyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2V0UGFzcyA9IGZhbHNlOyAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHRoaXMucmVzZXRFbWFpbCA9IFwiXCI7XHJcbiAgICB9XHJcbiAgICBsb2dpbkFjdGlvbigpXHJcbiAgICB7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmxvZ2luUGFzcyk7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmxvZ2luTWFpbCk7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmxvZ2luTWFpbCAhPSBcIlwiKTtcclxuICAgICAgICAgICAgaWYodGhpcy5sb2dpblBhc3MgJiYgdGhpcy5sb2dpbk1haWwpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGVtcGxhdGVWaXNpYmlsaXR5ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIG15R2xvYmFscy50ZW1wbGF0ZVZpc2liaWxpdHkgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXHJcbi8vICAgICAgICAgbGV0IGxpbmsgPSBbJ1Rlc3RSb3V0J107XHJcbi8vICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUobGluayk7XHJcbiAgICB9XHJcbiAgICBsb2dPdXRBY3Rpb24oKVxyXG4gICAge1xyXG4gICAgICAgIHRoaXMudGVtcGxhdGVWaXNpYmlsaXR5ID0gZmFsc2U7XHJcbi8vICAgICAgICBjb25zb2xlLmxvZyhcImxvZyBvdXQgYWN0aW9uXCIpOyAgICAgICAgXHJcbi8vICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmxvZ2luQ29tcG9uZW50KTsgICAgXHJcbi8vICAgIHRoaXMubG9naW5Db21wb25lbnQubG9naW5BY3Rpb24oKTtcclxuICAgICAgICBcclxuICAgIH1cclxufSJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
