System.register(["@angular/core", '@angular/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            LoginComponent = (function () {
                //let router = Router;
                function LoginComponent(router) {
                    this.router = router;
                    this.pasword = "";
                    this.forgetPass = true;
                    this.signup = true;
                    this.login = false;
                }
                LoginComponent.prototype.checkPass = function (val) {
                    console.log('at the changes check');
                    if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(this.pasword)) {
                        console.log(2);
                        this.passwordStrength = 2;
                    }
                    else if (/^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/.test(this.pasword)) {
                        console.log(1);
                        this.passwordStrength = 1;
                    }
                    else {
                        console.log(0);
                        this.passwordStrength = 0;
                    }
                };
                LoginComponent.prototype.showForgetPassword = function () {
                    this.forgetPass = false;
                    this.signup = true;
                    this.login = true;
                };
                LoginComponent.prototype.showSignup = function () {
                    this.forgetPass = true;
                    this.signup = false;
                    this.login = true;
                };
                LoginComponent.prototype.showLogin = function () {
                    this.forgetPass = true;
                    this.signup = true;
                    this.login = false;
                };
                LoginComponent.prototype.loginAction = function () {
                    var link = ['TestRout'];
                    this.router.navigate(link);
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        selector: "login",
                        templateUrl: "dev/login/login.tpl.html",
                        styleUrls: ["assets/pages/css/login-5.css"]
                    }), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_1("LoginComponent", LoginComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luL2xvZ2luLmNvbXBvbmVudCAtIENvcHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFVQTtnQkFNQSxzQkFBc0I7Z0JBRXRCLHdCQUNRLE1BQWM7b0JBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtvQkFQbEIsWUFBTyxHQUFHLEVBQUUsQ0FBQztvQkFDYixlQUFVLEdBQUcsSUFBSSxDQUFDO29CQUNsQixXQUFNLEdBQUcsSUFBSSxDQUFDO29CQUNkLFVBQUssR0FBRyxLQUFLLENBQUM7Z0JBS2xCLENBQUM7Z0JBQ0Qsa0NBQVMsR0FBVCxVQUFVLEdBQUc7b0JBRVQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO29CQUNwQyxFQUFFLENBQUEsQ0FBQyxnRUFBZ0UsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FDckYsQ0FBQyxDQUFELENBQUM7d0JBQ0csT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDZixJQUFJLENBQUMsZ0JBQWdCLEdBQUUsQ0FBQyxDQUFDO29CQUM3QixDQUFDO29CQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyx3RkFBd0YsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQ3JILENBQUM7d0JBQ0csT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDZixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO29CQUM5QixDQUFDO29CQUNELElBQUksQ0FBQSxDQUFDO3dCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2YsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQztvQkFDMUIsQ0FBQztnQkFDTCxDQUFDO2dCQUNELDJDQUFrQixHQUFsQjtvQkFDSSxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUN0QixDQUFDO2dCQUNELG1DQUFVLEdBQVY7b0JBQ0ksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztnQkFDdEIsQ0FBQztnQkFDRCxrQ0FBUyxHQUFUO29CQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29CQUN2QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztvQkFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLENBQUM7Z0JBQ0csb0NBQVcsR0FBWDtvQkFFSSxJQUFJLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDL0IsQ0FBQztnQkF0REw7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDUCxRQUFRLEVBQUMsT0FBTzt3QkFDaEIsV0FBVyxFQUFDLDBCQUEwQjt3QkFDdEMsU0FBUyxFQUFDLENBQUMsOEJBQThCLENBQUM7cUJBQ3pDLENBQUM7O2tDQUFBO2dCQW1ETixxQkFBQztZQUFELENBakRBLEFBaURDLElBQUE7WUFqREQsMkNBaURDLENBQUEiLCJmaWxlIjoibG9naW4vbG9naW4uY29tcG9uZW50IC0gQ29weS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6XCJsb2dpblwiLFxyXG4gICAgdGVtcGxhdGVVcmw6XCJkZXYvbG9naW4vbG9naW4udHBsLmh0bWxcIixcclxuICAgIHN0eWxlVXJsczpbXCJhc3NldHMvcGFnZXMvY3NzL2xvZ2luLTUuY3NzXCJdXHJcbiAgICB9KVxyXG5cclxuZXhwb3J0ICBjbGFzcyBMb2dpbkNvbXBvbmVudHtcclxuXHJcbiAgICBwYXN3b3JkID0gXCJcIjtcclxuICAgIGZvcmdldFBhc3MgPSB0cnVlO1xyXG4gICAgc2lnbnVwID0gdHJ1ZTtcclxuICAgIGxvZ2luID0gZmFsc2U7XHJcbi8vbGV0IHJvdXRlciA9IFJvdXRlcjtcclxuXHJcbmNvbnN0cnVjdG9yKFxyXG5wcml2YXRlIHJvdXRlcjogUm91dGVyKSB7XHJcbn1cclxuY2hlY2tQYXNzKHZhbClcclxue1xyXG4gICAgY29uc29sZS5sb2coJ2F0IHRoZSBjaGFuZ2VzIGNoZWNrJyk7XHJcbiAgICBpZigvXig/PS4qW2Etel0pKD89LipbQS1aXSkoPz0uKlswLTldKSg/PS4qWyFAI1xcJCVcXF4mXFwqXSkoPz0uezgsfSkvLnRlc3QodGhpcy5wYXN3b3JkKVxyXG4gICAge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKDIpO1xyXG4gICAgICAgIHRoaXMucGFzc3dvcmRTdHJlbmd0aD0gMjtcclxuICAgIH1cclxuICAgIGVsc2UgaWYgKC9eKCgoPz0uKlthLXpdKSg/PS4qW0EtWl0pKXwoKD89LipbYS16XSkoPz0uKlswLTldKSl8KCg/PS4qW0EtWl0pKD89LipbMC05XSkpKSg/PS57Nix9KS8udGVzdCh0aGlzLnBhc3dvcmQpKVxyXG4gICAge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKDEpO1xyXG4gICAgICAgIHRoaXMucGFzc3dvcmRTdHJlbmd0aCA9IDE7XHJcbiAgICB9XHJcbiAgICBlbHNle1xyXG4gICAgY29uc29sZS5sb2coMCk7XHJcbiAgICB0aGlzLnBhc3N3b3JkU3RyZW5ndGggPSAwO1xyXG4gICAgfVxyXG59XHJcbnNob3dGb3JnZXRQYXNzd29yZCgpe1xyXG4gICAgdGhpcy5mb3JnZXRQYXNzID0gZmFsc2U7XHJcbiAgICB0aGlzLnNpZ251cCA9IHRydWU7XHJcbiAgICB0aGlzLmxvZ2luID0gdHJ1ZTtcclxufVxyXG5zaG93U2lnbnVwKCl7XHJcbiAgICB0aGlzLmZvcmdldFBhc3MgPSB0cnVlO1xyXG4gICAgdGhpcy5zaWdudXAgPSBmYWxzZTtcclxuICAgIHRoaXMubG9naW4gPSB0cnVlO1xyXG59XHJcbnNob3dMb2dpbigpe1xyXG4gICAgdGhpcy5mb3JnZXRQYXNzID0gdHJ1ZTtcclxuICAgIHRoaXMuc2lnbnVwID0gdHJ1ZTtcclxuICAgIHRoaXMubG9naW4gPSBmYWxzZTtcclxufVxyXG4gICAgbG9naW5BY3Rpb24oKVxyXG4gICAge1xyXG4gICAgICAgIGxldCBsaW5rID0gWydUZXN0Um91dCddO1xyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKGxpbmspO1xyXG4gICAgfVxyXG59XHJcbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
