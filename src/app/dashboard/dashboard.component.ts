import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import {HeaderComponent} from "../header/header.component";
import {SideMenuComponent} from "../sideMenu/sideMenu.component";
import {FooterComponent} from "../footer/footer.component";
//import myGlobals = require('../globals');
//import {AppComponent} from '../app.component';

//import { Router } from '@angular/router-deprecated';

@Component({
    moduleId: module.id,
    selector:"dashboard",
    templateUrl:"dashboard.tpl.html",
    directives: [ROUTER_DIRECTIVES,HeaderComponent,SideMenuComponent,FooterComponent]
    })

export  class DashboardComponent implements OnInit{

}