import { RouterConfig } from '@angular/router';

import {DashboardComponent} from "./dashboard.component";
export const DashboardRoutes: RouterConfig = [
  { path: '', terminal: true, redirectTo: '/dashboard' },
  { path: 'dashboard', component: DashboardComponent}
];