import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import {HeaderComponent} from "../header/header.component";
import {SideMenuComponent} from "../sideMenu/sideMenu.component";
import {ProSideMenuComponent} from "../proSideMenu/proSideMenu.component";
import {FooterComponent} from "../footer/footer.component";
//import myGlobals = require('../globals');
//import {AppComponent} from '../app.component';

//import { Router } from '@angular/router-deprecated';

@Component({
    moduleId: module.id,
    selector:"profile",
    templateUrl:"profile.tpl.html",    
    styleUrls:["../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"],    
    directives: [ROUTER_DIRECTIVES,HeaderComponent,SideMenuComponent,ProSideMenuComponent,FooterComponent]
    })

export class ProfileComponent implements OnInit{

}