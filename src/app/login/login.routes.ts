import { RouterConfig } from '@angular/router';

import {LoginComponent} from "./login.component";
export const LoginRoutes: RouterConfig = [
  { path: '', terminal: true, redirectTo: '/login' },
  { path: 'login', component: LoginComponent}
];