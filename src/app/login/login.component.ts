import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { Router, ActivatedRoute } from '@angular/router';
import {HeaderComponent} from "../header/header.component";
import {SideMenuComponent} from "../sideMenu/sideMenu.component";
import {FooterComponent} from "../footer/footer.component";
//import myGlobals = require('../globals');
//import {AppComponent} from '../app.component';

//import { Router } from '@angular/router-deprecated';

@Component({
    moduleId: module.id,
    selector:"login",
    templateUrl:"login.tpl.html",
    styleUrls:["../../assets/pages/css/login-5.css"],
directives: [ROUTER_DIRECTIVES]
//,HeaderComponent,SideMenuComponent,FooterComponent]
    })

export  class LoginComponent implements OnInit{
////    passwordStrength=0;

constructor(private router: Router){
}

    registerFormIsValid = false;
    templateVisibility = false;
    confirmPassword = "";
    emailAddressValidation = '';
    emailAddress = "";
    emailAddressConfirmation = "";
    password = "";
    forgetPass = true;
    signup = true;
    login = false;
    resetPass = true;
    signupConfirm= true;



    ngOnInit()
    {
        console.log("-----------------------")  ;
        console.log("myGlobals");
    }



//    console.log(2);
//    console.log("-----------------------");
    checkFormValidation()
    {
    if( this.emailAddressValidation === 1 
&& this.confirmPassword === this.password
        && this.username != ''
        && this.displayname != ''
&& this.emailAddressConfirmation ===  this.emailAddress
            )
    {
            return true;
    }
else{
    this.confirmPassword = null;
    this.emailAddressValidation = 0;
    this.emailAddressConfirmation = " ";
        return false;
}


    }
    checkMailForamte (val)
    {
            if(/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/.test(this.emailAddress))
                this.emailAddressValidation = 1;
            else
                this.emailAddressValidation = 0;
    }
    checkPass(val)
    {
        console.log('at the changes check');
    if(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(this.password)
    {
        console.log(2);
        this.passwordStrength= 2;
    }
    else if (/^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/.test(this.password))
    {
        console.log(1);
        this.passwordStrength = 1;
    }
else{
    console.log(0);
    this.passwordStrength = 0;
}
    }
showForgetPassword(){
    this.forgetPass = false;
    this.signup = true;
    this.login = true;
}
showSignup(){
    this.forgetPass = true;
    this.signup = false;
    this.login = true;
}
showLogin(){
    this.forgetPass = true;
    this.signup = true;
    this.login = false;
    this.forgetPass = true;
    this.resetPass = true;
    this.signupConfirm = true;
}
signupConfirmAction(){
        if(this.checkFormValidation())
        {       
            this.signup=true;
            this.signupConfirm=false;
        }
}
resetPassAction(){
        if(/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/.test(this.resetEmail))
        {
            this.forgetPass = true;
            this.resetPass = false;   
        }
        else
            this.resetEmail = "";
}
    loginAction()
    {
        console.log(this.loginPass);
        console.log(this.loginMail);
        console.log(this.loginMail != "");
            if(this.loginPass && this.loginMail)
            {
                this.templateVisibility = true;
                this.router.navigate(['/dashboard']);
//                myGlobals.templateVisibility = true;
            }

//         let link = ['TestRout'];
//         this.router.navigate(link);
    }
    logOutAction()
    {
        this.templateVisibility = false;
//        console.log("log out action");        
//        console.log(this.loginComponent);    
//    this.loginComponent.loginAction();

    }
}