import {Component} from "@angular/core";

import { Router, ActivatedRoute } from '@angular/router';
import {LoginComponent} from "../login/login.component";
//import { Router } from '@angular/router-deprecated';

@Component({
    selector:"header",
    templateUrl:"./src/app/header/header.tpl.html"
//    styleUrls:["assets/pages/css/login-5.css"]
    })

export  class HeaderComponent{

constructor(private router: Router){
}
    logOutAction(LoginComponent:loginComponent)
    {
        console.log("log out action");      
        this.router.navigate(['/login']);

    }

}