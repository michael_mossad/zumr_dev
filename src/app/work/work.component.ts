import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import {HeaderComponent} from "../header/header.component";
import {SideMenuComponent} from "../sideMenu/sideMenu.component";
import {ProSideMenuComponent} from "../proSideMenu/proSideMenu.component";
import {FooterComponent} from "../footer/footer.component";
//import myGlobals = require('../globals');
//import {AppComponent} from '../app.component';

//import { Router } from '@angular/router-deprecated';

@Component({
    moduleId: module.id,
    selector:"work",
    templateUrl:"work.tpl.html"
directives: [ROUTER_DIRECTIVES
,HeaderComponent,SideMenuComponent,FooterComponent,ProSideMenuComponent
]
    })

export  class WorkComponent implements OnInit{
////    passwordStrength=0;
workTemp =  {
}
workList = [
    {
        work : "work1",
        category : "category1"
    },
    {
        work : "work2",
        category : "category2"
    }

]
    addNewWork()
    {
            if (this.workTemp.category ==='1')
                this.workTemp.category = "Category 1";
            if (this.workTemp.category ==='2')
                this.workTemp.category = "Category 2";
        this.workList.push(this.workTemp);   
        this.workTemp = {};
    }

    templateVisibility = true;
 



constructor(){
}
    logOutAction()
    {
        this.templateVisibility = false;

    }
}