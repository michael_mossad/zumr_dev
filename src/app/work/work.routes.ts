import { RouterConfig } from '@angular/router';

import {WorkComponent} from "./work.component";
export const WorkRoutes: RouterConfig = [
  { path: '', terminal: true, redirectTo: '/work' },
  { path: 'work', component: WorkComponent}
];