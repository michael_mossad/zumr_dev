import { provideRouter, RouterConfig } from '@angular/router';
import { LoginRoutes } from './login/login.routes';
import { WorkRoutes } from './work/work.routes';
import { DashboardRoutes } from './dashboard/dashboard.routes';
import { ProfileRoutes } from './profile/profile.routes';
//import { TestRoutRoutes } from './testRout/testRout.routes';

export const App_Routes: RouterConfig = [
  
  ...LoginRoutes, 
  ...WorkRoutes,
  ...DashboardRoutes,
  ...ProfileRoutes,
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(App_Routes)
];